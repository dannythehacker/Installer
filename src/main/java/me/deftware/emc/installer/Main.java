package me.deftware.emc.installer;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;
import me.deftware.emc.installer.ui.InstallerUI;
import me.deftware.emc.installer.utils.Cleaner;
import me.deftware.emc.installer.utils.WebUtils;

import javax.swing.*;

public class Main {

    private static final String aristoisURL = "https://aristois.net/maven/update.json";
    public static String customEMC = "", customAristois = "";
    public static boolean optifine = true;
    private static String versionsURL = "https://gitlab.com/EMC-Framework/maven/raw/master/versions.json";

    public static void main(String[] args) {
        try {
            OptionParser optionParser = new OptionParser();
            optionParser.allowsUnrecognizedOptions();
            OptionSpec<String> emcVersion = optionParser.accepts("emc").withOptionalArg();
            OptionSpec<String> aristoisVersion = optionParser.accepts("aristois").withOptionalArg();
            OptionSpec<Boolean> devel = optionParser.accepts("dev").withOptionalArg().ofType(Boolean.class).describedAs("bool");
            OptionSpec<Boolean> optifine = optionParser.accepts("of").withOptionalArg().ofType(Boolean.class).describedAs("bool");
            OptionSpec<Boolean> clean = optionParser.accepts("clean").withOptionalArg().ofType(Boolean.class).describedAs("bool");
            OptionSpec<Boolean> deepclean = optionParser.accepts("deepclean").withOptionalArg().ofType(Boolean.class).describedAs("bool");
            OptionSet optionSet = optionParser.parse(args);
            if (optionSet.has(emcVersion)) {
                System.out.println("Using custom EMC version " + optionSet.valueOf(emcVersion));
                customEMC = optionSet.valueOf(emcVersion);
            }
            if (optionSet.has(devel)) {
                System.out.println("Using development json");
                versionsURL = "https://gitlab.com/EMC-Framework/maven/raw/master/versions-devel.json";
            }
            if (optionSet.has(aristoisVersion)) {
                System.out.println("Using custom Aristois version " + optionSet.valueOf(aristoisVersion));
                customAristois = optionSet.valueOf(aristoisVersion);
            }
            if (optionSet.has(optifine)) {
                System.out.println(optionSet.valueOf(optifine) ? "Using OptiFine" : "Installer will not install OptiFine");
                Main.optifine = optionSet.valueOf(optifine);
            }
            if (optionSet.has(clean) || optionSet.has(deepclean)) {
                Cleaner.clean(optionSet.has(deepclean));
            }
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            new Thread(() -> {
                try {
                    System.out.print("Fetching json files... ");
                    JsonObject emcJson = new Gson().fromJson(WebUtils.get(versionsURL), JsonObject.class),
                            aristoisJson = new Gson().fromJson(WebUtils.get(aristoisURL), JsonObject.class);
                    System.out.println("Done");
                    InstallerUI.create(emcJson, aristoisJson).setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
