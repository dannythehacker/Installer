package me.deftware.emc.installer.utils;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.File;

public class DependencyInstaller {

    public DependencyInstaller(JsonObject json, String mcVersion) throws Exception {
        System.out.println("Installing dependencies...");
        File EMC_LIBS = new File(Utils.getMinecraftRoot() + "libraries" + File.separator + "EMC" + File.separator + mcVersion + File.separator);
        JsonArray array = json.get("dependencies").getAsJsonArray();
        array.forEach(dependency -> {
            JsonObject data = dependency.getAsJsonObject();
            String[] name = data.get("name").getAsString().split(":");
            String url = data.get("url").getAsString();
            File file = new File(EMC_LIBS.getAbsolutePath() + File.separator + name[1] + ".jar");
            if (file.exists() && !file.delete()) {
                System.out.println("Unable to delete previous version of " + name[1]);
            }
            url += name[0].replace(".", "/") + "/" + name[1] + "/" + name[2] + "/" + name[1] + "-" + name[2] + ".jar";
            try {
                WebUtils.download(url, file.getAbsolutePath());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            System.out.println("Installed " + name[1]);
        });
    }

}
